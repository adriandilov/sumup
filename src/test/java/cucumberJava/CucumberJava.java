package cucumberJava;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.annotation.Before;
import cucumber.annotation.en.Given;
import cucumber.annotation.en.Then;
import cucumber.annotation.en.When;

public class CucumberJava {
	WebDriver driver = null;

	@Before
	public void openBrowser() {

		System.setProperty("webdriver.gecko.driver", "C:\\firefox_driver\\geckodriver-v0.26.0-win64\\geckodriver.exe");
		driver = new FirefoxDriver();
	}

	@Given("^I am on the main page$")
	public void OpenTheMainPage() {
		// Initiate web browser instance. driver = new FirefoxDriver();
		driver.navigate().to("https://help.sumup.com/en-GB");
		
		// explicit wait
		//WebDriverWait wait = new WebDriverWait(driver, 10);
		//WebElement alert = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("cmp-faktor-io")));

		//WebElement frameElement = driver.findElement(By.id("cmp-faktor-io"));

		//driver.switchTo().frame(frameElement);
		
		//Alert alert1 = driver.switchTo().alert();
		
		//alert1.accept();
	}

	@When("^I access the United States help page and search for a term$")
	public void FindTheUnitedStatesHelpPage () {

		// Enter data
		driver.findElement(By.xpath("/html/body/div[1]/footer/div[1]/div/div[2]/div[2]/div/div/div")).click();
		driver.findElement(By.id("react-select-3-option-33")).click();
		driver.findElement(By.id("query")).sendKeys("invoice");
		driver.findElement(By.id("query")).sendKeys(Keys.ENTER);

		//driver.findElement(By.xpath("//*/input[@value='Ð¡ÑŠÐ·Ð´Ð°Ð¹ Ð�Ð‘Ð’ ÐŸÑ€Ð¾Ñ„Ð¸Ð»']")).click();
	}

	@Then("^I will see the results and I can choose one$")
	public void SelectResult() {
		
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		String text = driver.findElement(By.xpath("//main/div/div/div[1]/p")).getText();
		int searchResultsNumber = Integer.parseInt(text.replaceAll("[^0-9]", ""));
		
		if (searchResultsNumber == 23) {
			System.out.println("Test Pass");
		} else {
			System.out.println("Test Failed");
		}
		
		driver.findElement(By.xpath("/html/body/main/div/div/div[2]/nav/ul/li[3]")).click();
		
		String headerOfLastSearchResult = driver.findElement(By.xpath("/html/body/main/div/div/div[1]/section/div[3]/a/h2")).getText();
		
		driver.findElement(By.xpath("/html/body/main/div/div/div[1]/section/div[3]/a/h2")).click();
		
		String headerOfSection = driver.findElement(By.xpath("/html/body/main/div/div/div[2]/article/header/h1")).getText();
		
		if (headerOfLastSearchResult.equals(headerOfSection)) {
			System.out.println("Test Pass");
		} else {
			System.out.println("Test Failed");
		}
		
		driver.findElement(By.xpath("/html/body/div[1]/header/div[1]/div/div[2]/a[1]/span")).click();
		
		//driver.close();
	}

}