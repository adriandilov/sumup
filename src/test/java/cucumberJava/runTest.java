package cucumberJava;

import org.junit.runner.RunWith;
import cucumber.junit.Cucumber;

//@RunWith annotation: This is a JUnit annotation that specifies which runner it has to use to execute this class. 
//You can see that we have provided Cucumber.class as a parameter with this annotation. 
//With this, JUnit will know that it has to execute this test case as a Cucumber test.
@RunWith(Cucumber.class)

//@CucumberOptions annotation: This annotation provides some important information which will be used to run your cucumber feature file.
//At the very least, Java should know the location of the feature file as well as the step definition class in your project. 
//CucumberOptions annotation will do just that. 
//As of now, we have provided two parameters in this annotation. 
//@CucumberOptions(features="<path of feature file>", glue="<path of step defs class>")

//@Cucumber.Options(tags = {"@CucumberJava"}, format = { "pretty", "html:target/cucumber-html-report" }, glue = "cucumberJava")
@Cucumber.Options(features = { "features/" },format = { "pretty", "json:target/cucumber-json-report.json" }, glue = "cucumberJava", monochrome = true)

public class runTest {
}

